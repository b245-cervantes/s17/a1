/*
	1. Create a function which is able to prot the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let myUser = function() {
		alert("Hi! Welcome to this page, Please fill up the following question.");
		let fullName = prompt("Input your full name:  ");
		let age = prompt("Input your age: ");
		let location = prompt("Input your location: ");
		alert("Thank you for your Input!");

		console.log(fullName);
		console.log(age);
		console.log(location);
	}
	myUser();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	let favBand = function() {
		let myFaveBand = [
         "The Beatles",
         "Metallica",
         "The Eagles",
         "L'arc~en~Ciel",
         "Eraserheads"

		];
		console.log("1."+myFaveBand[0]);
		console.log("2."+myFaveBand[1]);
		console.log("3."+myFaveBand[2]);
		console.log("4."+myFaveBand[3]);
		console.log("5."+myFaveBand[4]);
	}
	favBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	let favMovie = function() {
		let myFavMovie = [

		{title: "The Godfather"},
		{rating: "97%"},
		{title: "The Godfather, Part II"},
		{rating: "96%"},
		{title: "Shawshank Redemption"},
		{rating: "91%"},
		{title: "To kill A Mockingbird"},
		{rating: "93%"},
		{title: "Psycho"},
		{rating: "96%"},


		]
	   let fMovie = Object.values(myFavMovie[0]);
	   let rating = Object.values(myFavMovie[1]);

	   let fMovie2 = Object.values(myFavMovie[2]);
	   let rating2 = Object.values(myFavMovie[3]);

	   let fMovie3 = Object.values(myFavMovie[4]);
	   let rating3 = Object.values(myFavMovie[5]);

	   let fMovie4 = Object.values(myFavMovie[6]);
	   let rating4 = Object.values(myFavMovie[7]);

	   let fMovie5 = Object.values(myFavMovie[8]);
	   let rating5 = Object.values(myFavMovie[9]);
		
        // console.log(`1. ${myFavMovie[Object.entries(myFavMovie)[0]]}\nRotten Tomatoes Rating: ${myFavMovie[1]}`);
		console.log(`1. ${fMovie}\nRotten Tomatoes rating: ${rating}`);
		console.log(`2. ${fMovie2}\nRotten Tomatoes rating: ${rating2}`);
		console.log(`3. ${fMovie3}\nRotten Tomatoes rating: ${rating3}`);
		console.log(`4. ${fMovie4}\nRotten Tomatoes rating: ${rating4}`);
		console.log(`5. ${fMovie5}\nRotten Tomatoes rating: ${rating5}`);
	}
	favMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
